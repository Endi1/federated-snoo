# Federated Snoo

Federated (ActivityPub) Hackernews

## Client-to-server
The client sends a `POST` request to the outbox with the following Content-Type:
`application/ld+json; profile="https://www.w3.org/ns/activitystreams"`

The request MUST be authenticated with the credentials of the user.

The body of the `POST` must contain a single Activity that has an embedded
object.

All activities MUST be sent to `https://www.w3.org/ns/activitystreams#Public`

The following activities are supported

### Create
#### Document
This is when submitting a link
```
{
	"@context":"https://www.w3.org/ns/activitystreams",
	"type": "Create",
	"actor": "http://localhost:8000/users/Endi",
	"to": "https://www.w3.org/ns/activitystreams#Public",
	"object": {
		"type": "Document",
		"url": "http://example.org",
		"title": "This is another title"
	},
    "published": "2015-02-10T15:04:55Z"
}
```
#### Article
This is basically a self-post
```
{
	"@context":"https://www.w3.org/ns/activitystreams",
	"type": "Create",
	"actor": "http://localhost:8000/users/Endi",
	"to": "https://www.w3.org/ns/activitystreams#Public",
	"object": {
		"type": "Article",
		"title": "This is a title",
        "content": "<h1>This is the header</h1>",
        "source": {
        "content": "# This is the header",
        "mediaType": "text/markdown"
        }
	},
    "published": "2015-02-10T15:04:55Z"
}

```
### Update
This is used to update an existing object, should only support updating article
bodies
```
{
	"@context":"https://www.w3.org/ns/activitystreams",
	"type": "Update",
	"actor": "http://localhost:8000/users/Endi",
	"to": "https://www.w3.org/ns/activitystreams#Public",
	"object": {
		"type": "Article",
		"title": "This is a title",
        "content": "<h1>This is the updated header</h1>",
        "source": {
        "content": "# This is the updated header",
        "mediaType": "text/markdown"
        }
	},
    "published": "2015-02-10T15:04:55Z"
}
```
### Delete
Delete an existing object. After deletion, the object should return a Tombstone
and when requested the server should respond with `HTTP 410 Gone status code`

### Like
```
{
  "@context": ["https://www.w3.org/ns/activitystreams"],
  "type": "Like",
  "actor": "https://dustycloud.org/chris/",
  "summary": "Chris liked 'Minimal ActivityPub update client'",
  "object": "https://rhiaro.co.uk/2016/05/minimal-activitypub",
  "to": "https://www.w3.org/ns/activitystreams#Public"
}
```
