* Federated Snoo
  Federated (ActivityPub) Hackernews
** TODO Find a new name
** TODO If I create a new activity in another federated server, there's no need to create a thread item
** DONE Firehose
** Client-to-server

   The client sends a `POST` request to the outbox with the following Content-Type:
   `application/ld+json; profile="https://www.w3.org/ns/activitystreams"`

   The request MUST be authenticated with the credentials of the user.

   The body of the `POST` must contain a single Activity that has an embedded
   object.

   All activities MUST be sent to `https://www.w3.org/ns/activitystreams#Public`

   The following activities are supported
   
*** Create
**** Document
     This is when submitting a link
     #+BEGIN_SRC json
     {
	   "@context":"https://www.w3.org/ns/activitystreams",
	   "type": "Create",
	   "actor": "http://localhost:8000/users/Endi",
	   "to": "https://www.w3.org/ns/activitystreams#Public",
	   "object": {
		 "type": "Document",
		 "url": "http://example.org",
		 "title": "This is another title"
	   },
     "published": "2015-02-10T15:04:55Z"
     }
     #+END_SRC

**** Article
   #+BEGIN_SRC json
    {
	  "@context":"https://www.w3.org/ns/activitystreams",
	  "type": "Create",
	  "actor": "http://localhost:8000/users/Endi",
	  "to": "https://www.w3.org/ns/activitystreams#Public",
	  "object": {
		"type": "Article",
		"title": "This is a title",
    "content": "<h1>This is the header</h1>",
    "source": {
    "content": "# This is the header",
    "mediaType": "text/markdown"
    }
	  },
    "published": "2015-02-10T15:04:55Z"
    }
    #+END_SRC
     This is basically a self-post
***** DONE Write
***** DONE Test
*** Update
    #+BEGIN_SRC json
    {
	  "@context":"https://www.w3.org/ns/activitystreams",
	  "type": "Update",
	  "actor": "http://localhost:8000/users/Endi",
	  "to": "https://www.w3.org/ns/activitystreams#Public",
	  "object": {
	  "type": "Article",
    "uri": "uri"
		"title": "This is a title",
    "content": "<h1>This is the updated header</h1>",
    "source": {
    "content": "# This is the updated header",
    "mediaType": "text/markdown"
    }
	  },
    "published": "2015-02-10T15:04:55Z"
    }
    #+END_SRC
**** DONE Write
**** DONE Test
*** Like
    #+BEGIN_SRC json
    {
    "@context": ["https://www.w3.org/ns/activitystreams"],
    "type": "Like",
    "actor": "https://dustycloud.org/chris/",
    "summary": "Chris liked 'Minimal ActivityPub update client'",
    "object": "https://rhiaro.co.uk/2016/05/minimal-activitypub",
    "to": "https://www.w3.org/ns/activitystreams#Public"
    }
    #+END_SRC
**** DONE Write
**** DONE Test
*** DONE Dislike
#+BEGIN_SRC json
    {
    "@context": ["https://www.w3.org/ns/activitystreams"],
    "type": "Dislike",
    "actor": "https://dustycloud.org/chris/",
    "summary": "Chris liked 'Minimal ActivityPub update client'",
    "object": "https://rhiaro.co.uk/2016/05/minimal-activitypub",
    "to": "https://www.w3.org/ns/activitystreams#Public"
    }
#+END_SRC
*** Delete
    Delete an existing object. After deletion, the object should return a
    Tombstone and when requested the server should respond with `HTTP 410 Gone
    status code`
**** DONE Write
**** DONE Test
** Server-to-server
An activity is sent as a `POST` request to the target inbox.

The request header will have a signature and a way to get the public key.
The string to be signed is:
  #+BEGIN_SRC
   (request-target): post /inbox
   host: localhost:8000
   date: Sun, 06 Nov 1994 08:49:37 GMT
   #+END_SRC

The signature looks like:
  #+BEGIN_SRC 
   Signature: keyId="http://localhost:8000/user/Endi#main-key",headers="(request-target) host date",signature="..."
   #+END_SRC
*** TODO Webfinger
