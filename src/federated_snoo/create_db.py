from app import db
from models.account import Account
from models.input_stream import InputStream
from models.thread_item import ThreadItem
from models.output_stream import OutputStream


db.create_all()
