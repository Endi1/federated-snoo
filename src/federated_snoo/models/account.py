from app import db
from .input_stream import InputStream
"""
Table name: accounts
id
username
email
password
salt
private_key
public_key
created_at
updated_at
display_name
inbox_url
outbox_url
following_url
actor_type
"""


class Account(db.Model):
    __tablename__ = 'accounts'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(160), unique=True, nullable=False)
    email = db.Column(db.String(160), unique=True, nullable=False)
    password = db.Column(db.String(240), nullable=False)
    salt = db.Column(db.String(240), nullable=False)
    private_key = db.Column(db.String(240), nullable=False)
    public_key = db.Column(db.String(240), nullable=False)
    created_at = db.Column(db.String(160))
    updated_at = db.Column(db.String(160))
    display_name = db.Column(db.String(160))
    inbox_url = db.Column(db.String(240), nullable=False)
    outbox_url = db.Column(db.String(240), nullable=False)
    following_url = db.Column(db.String(240), nullable=False)
    actor_type = db.Column(db.String(20), nullable=False)
    entries = db.relationship('InputStream', backref='account', lazy=True)
