from app import db
"""
id
activity_id
activity_type
created_at
updated_at
account_id
"""


class InputStream(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    activity_id = db.Column(db.Integer, db.ForeignKey('thread_items.id'))
    activity_type = db.Column(db.String(240), nullable=False)
    created_at = db.Column(db.String(240), nullable=False)
    updated_at = db.Column(db.String(240), nullable=False)
    account_id = db.Column(db.Integer, db.ForeignKey('accounts.id'))
    account_uri = db.Column(db.String(480))
