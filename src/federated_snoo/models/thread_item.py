from app import db
"""
id
uri
title
text
posted_url
created_at
updated_at
in_reply_to_id
nsfw
account_id
"""


class ThreadItem(db.Model):
    __tablename__ = 'thread_items'
    id = db.Column(db.Integer, primary_key=True)
    uri = db.Column(db.String(240), nullable=False, unique=True)

    # Either the account_id or account_uri need to be filled, if it's a user
    # signed into this instance, the account_id needs to be filled
    account_id = db.Column(db.Integer, db.ForeignKey('accounts.id'))
    account_uri = db.Column(db.String(480))

    title = db.Column(db.String(240), nullable=False)
    text = db.Column(db.String(16000))
    source = db.Column(db.String(16000))
    deleted = db.Column(db.Boolean, default=False)
    likes = db.Column(db.Integer, default=1)
    dislikes = db.Column(db.Integer, default=0)
    # If it's a Document type, this is the url
    posted_url = db.Column(db.String(320))
    created_at = db.Column(db.String(240), nullable=False, unique=True)
    updated_at = db.Column(db.String(240), nullable=False, unique=True)
    # If it doesn't have a parent_id then it's a Post
    parent_id = db.Column(db.Integer, db.ForeignKey('thread_items.id'))
    parent = db.relationship('ThreadItem', backref='children',
                             lazy=True, uselist=False,
                             remote_side=[id])
