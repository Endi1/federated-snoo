from app import app
from blueprints import auth, account
from blueprints.firehose import firehose
from blueprints.outbox import outbox
from blueprints.inbox import inbox
from blueprints.webfinger import webfinger


app.register_blueprint(auth.auth_bp)
app.register_blueprint(outbox.outbox_bp)
app.register_blueprint(account.account_bp)
app.register_blueprint(inbox.inbox_bp)
app.register_blueprint(webfinger.webfinger_bp)
app.register_blueprint(firehose.firehose_bp)

if __name__ == '__main__':
    app.run(port=8000, debug=True)
