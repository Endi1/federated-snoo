from flask import Blueprint, jsonify, abort

from models.account import Account
from blueprints.constants import URI

account_bp = Blueprint('account', __name__, url_prefix='/users')


@account_bp.route('/<username>')
def showPublicKey(username):
    account = Account.query.filter_by(username=username).first()

    if not account:
        abort(404)

    result = {
        '@context': [
            'https://www.w3.org/ns/activitystreams',
            'https://w3id.org/security/v1'
        ],
        'id': URI + 'users/' + username,
        'type': 'Person',
        'preferredUsername': username,
        'inbox': URI + 'inbox',
        'publicKey': {
            'publicKeyPem': str(account.public_key)
        }
    }

    return jsonify(result)
