import re

from models.account import Account
from flask import Blueprint, jsonify, request, abort
from blueprints.constants import URI

webfinger_bp = Blueprint('webfinger', __name__, url_prefix='/.well-known')


@webfinger_bp.route('/webfinger')
def data():
    resource = request.args.get('resource')
    if not resource:
        abort(404)

    uri_match = re.match(r'acct\:(.+)', resource)

    if not uri_match:
        abort(404)

    acc_uri = uri_match.group(1).strip()
    if not acc_uri:

        abort(404)

    uri_regex = URI + 'users/(.+)'
    username_match = re.match(uri_regex, acc_uri)

    if not username_match:
        abort(404)

    username = username_match.group(1)

    user = Account.query.filter_by(username=username).first()

    if not user:
        abort(404)

    result = {
        'subject': URI + 'users/' + user.username,
        'links': [
            {
                'rel': 'self',
                'type': 'application/activity+json',
                'href': URI + 'users/'
            }
        ]
    }

    return jsonify(result)
