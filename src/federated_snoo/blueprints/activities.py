import datetime
import uuid

from app import db
from blueprints.constants import URI
from flask import make_response, jsonify, abort
from models.thread_item import ThreadItem
from models.output_stream import OutputStream
from models.input_stream import InputStream


def handleUpdateActivity(data_object, stream_type, current_session=None):
    """
    Handles an activity of 'Update' type.
    Keyword arguments:
    data_object -- the json object of the data to be saved 
    stream_type -- either 'Outbox' or 'Inbox'
    current_session -- The current session
    """
    # If there's no current_session, this is an inbox request. We need
    # to make sure that the actor that sent the request is the owner
    # of the item. For now we need to check both because federation is
    # not implemented yet.

    object_ = data_object['object']
    object_uri = object_['uri']
    thread_item = ThreadItem.query.filter_by(uri=object_uri).first()

    if not thread_item:
        abort(404)

    updated_at = str(datetime.datetime.now())

    thread_item.text = object_['content']
    thread_item.source = object_['source']['content']
    thread_item.updated_at = updated_at

    activity = buildActivity(stream_type, 'Update', thread_item,
                             current_session=current_session,
                             data_object=data_object)

    db.session.add(activity)
    db.session.commit()

    return createResponse(activity)


def handleLikeActivity(data_object, stream_type, current_session=None):
    """
    Handles an activity of 'Like' type.
    Keyword arguments:
    data_object -- the json object of the data to be saved 
    stream_type -- either 'Outbox' or 'Inbox'
    current_session -- The current session
    """
    object_uri = data_object['object']
    thread_item = ThreadItem.query.filter_by(uri=object_uri).first()
    type_ = data_object['type']

    # If this user has already liked this thread item, the like/dislike should
    # be removed
    if current_session:
        # The user is signed in and belongs to this instance. The data is in
        # the outbox
        output_streams = OutputStream.query.filter_by(
            account_id=current_session['id'], activity_type=type_
        ).all()

        if len(output_streams) % 2 == 0 and len(output_streams) != 0:
            _addLikeOrDislike(type_, -1, thread_item)
        else:
            _addLikeOrDislike(type_, 1, thread_item)
    else:
        input_streams = InputStream.query.filter_by(
            account_uri=data_object['actor'], activity_type=type_
        ).all()

        if len(input_streams) % 2 == 0 and len(input_streams) != 0:
            _addLikeOrDislike(type_, -1, thread_item)
        else:
            _addLikeOrDislike(type_, 1, thread_item)

    activity = buildActivity(stream_type, type_, thread_item,
                             current_session=current_session,
                             data_object=data_object)

    db.session.add(activity)
    db.session.commit()

    return createResponse(activity)


def _addLikeOrDislike(type_, amount, thread_item):
    if type_ == 'Like':
        thread_item.likes += amount
    elif type_ == 'Dislike':
        thread_item.dislikes += amount


def handleDeleteActivity(data_object, stream_type, current_session=None):
    """
    Handles an activity of 'Delete' type.
    Keyword arguments:
    data_object -- the json object of the data to be saved 
    stream_type -- either 'Outbox' or 'Inbox'
    current_session -- The current session
    """
    object_uri = data_object['object']
    thread_item = ThreadItem.query.filter_by(uri=object_uri).first()

    thread_item.deleted = True

    activity = buildActivity(stream_type,
                             'Delete',
                             thread_item,
                             current_session=current_session,
                             data_object=data_object)
    db.session.add(activity)
    db.session.commit()

    return createResponse(activity)


def handleCreateActivity(data_object, stream_type, current_session=None):
    """
    Handles an activity of 'Create' type.
    Keyword arguments:
    data_object -- the json object of the data to be saved 
    stream_type -- either 'Outbox' or 'Inbox'
    current_session -- The current session
    """

    thread_item = handleNewObject(data_object)

    input_object = buildActivity(stream_type, 'Create', thread_item,
                                 data_object=data_object,
                                 current_session=current_session)

    db.session.add(thread_item)
    db.session.add(input_object)
    db.session.commit()

    return createResponse(input_object)


def handleNewObject(data_object, session=None):
    object_ = data_object['object']
    uri = URI + 'thread/' + str(uuid.uuid4())
    created_at = str(datetime.datetime.now())

    thread_item = ThreadItem(
        uri=uri,
        account_uri=data_object['actor'],
        account_id=session['id'] if session else None,
        title=object_['title'],
        text=object_['content'] if 'content' in object_ else '',
        source=object_['source']['content'] if 'source' in object_ else '',
        posted_url=object_['url'] if 'url' in object_ else '',
        created_at=created_at,
        updated_at=created_at
    )

    if 'inReplyTo' in object_:
        return _addParent(thread_item, object_['inReplyTo'])

    return thread_item


def _addParent(thread_item, parent_uri):
    """Finds and adds the parent to the thread_item object"""
    parent = ThreadItem.query.filter_by(uri=parent_uri).first()
    thread_item.parent = parent
    return thread_item


def buildActivity(stream_type, activity_type,
                  thread_item, current_session=None, data_object={}):
    """Builds the activity to be saved in the outbox

    Keyword arguments:
    stream_type -- either 'Inbox' or 'Outbox'
    activity_type -- the activity type (Create, Update, Like, etc)
    current_session -- the session cookie
    thread_item -- the thread item created or updated by this activity
    data_object -- The activity object as a python dict
    """
    created_at = str(datetime.datetime.now())
    account_id = current_session['id'] if current_session else None
    account_uri = data_object['actor'] if data_object else None

    if stream_type == 'Outbox':
        activity = OutputStream(activity_type=activity_type,
                                created_at=created_at,
                                updated_at=created_at,
                                account_id=account_id,
                                account_uri=account_uri,
                                activity_object=thread_item)
    else:
        activity = InputStream(activity_type=activity_type,
                               created_at=created_at,
                               updated_at=created_at,
                               account_id=account_id,
                               account_uri=account_uri,
                               activity_object=thread_item)
    return activity


def createResponse(output_activity):
    response = make_response(
        jsonify({'activity_id': (output_activity.id)}), 201)
    response.headers['Location'] = URI + 'outbox/' + str(output_activity.id)

    return response


def _checkInboxAuth(actor, thread_item):
    # Just check if the actor in the activity is the same as the one
    # in the thread item. Since at this point the signature has been
    # verified, this is just to make sure that the actor sending the
    # request has ownership of the item
    if not (actor == thread_item.account_uri):
        return False
    return True
