from models.thread_item import ThreadItem
import datetime
import uuid
import json
from flask import make_response, jsonify, session, abort
from blueprints.constants import URI


def createResponse(output_activity):
    response = make_response(
        jsonify({'activity_id': (output_activity.id)}), 201)
    response.headers['Location'] = URI + 'outbox/' + str(output_activity.id)

    return response


def handleNewObject(data_object):
    object_ = data_object['object']
    uri = URI + 'thread/' + str(uuid.uuid4())
    created_at = str(datetime.datetime.now())

    thread_item = ThreadItem(
        uri=uri,
        account_uri=data_object['actor'],
        account_id=session['id'] if session else None,
        title=object_['title'],
        text=object_['content'] if 'content' in object_ else '',
        source=object_['source']['content'] if 'source' in object_ else '',
        posted_url=object_['url'] if 'url' in object_ else '',
        created_at=created_at,
        updated_at=created_at
    )

    if 'inReplyTo' in object_:
        return _addParent(thread_item, object_['inReplyTo'])

    return thread_item


def _addParent(thread_item, parent_uri):
    """Finds and adds the parent to the thread_item object"""
    parent = ThreadItem.query.filter_by(uri=parent_uri).first()

    if not parent:
        abort(404)

    thread_item.parent = parent
    return thread_item


def getDataObject(request):
    '''Converts the data sent over POST to a python dict

    Keyword arguments:
    request -- The flask Request object
    '''

    data = request.data.decode('utf-8')
    data_object = json.loads(data)

    return data_object
