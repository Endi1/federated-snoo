import re

from flask import Blueprint, request, abort, session
from blueprints.constants import URI
from .helpers import (getDataObject)

from .activities import (handleCreateActivity,
                         handleDeleteActivity,
                         handleLikeActivity,
                         handleUpdateActivity)

from ..validators import (content_type_validator,
                          activity_stream_validator,
                          user_authenticated_validator)
from .federation import handleFederationRequest

outbox_bp = Blueprint('outbox', __name__, url_prefix='/outbox')


# The content-type must be Content-Type of application/ld+json;
# profile="https://www.w3.org/ns/activitystreams".
@outbox_bp.route('/', methods=['POST'])
@content_type_validator
@activity_stream_validator
def outbox():
    # The outbox only accepts data from users so there must be a signed in user
    # in the session.
    user_authenticated_validator(session)
    data_object = getDataObject(request)

    # We need to check that the actor that sent this activity is the actor that
    # is signed in
    actor_username = _getActorUsername(data_object)
    if actor_username != session['username']:
        abort(401)

    activity_type = data_object['type']

    if _isAddressedToPublic(data_object):
        if activity_type == 'Create':
            return handleCreateActivity(data_object)
        elif activity_type == 'Update':
            return handleUpdateActivity(data_object)
        elif activity_type == 'Like' or activity_type == 'Dislike':
            return handleLikeActivity(data_object)
        elif activity_type == 'Delete':
            return handleDeleteActivity(data_object)
    else:
        handleFederationRequest(data_object, session)

    # This accepts either ThreadItem json-ld object or OutputStream json-ld
    # object. In any case, it must create and save both in the database and
    # then notify every user.
    return 'Outbox'


def _isAddressedToPublic(data_object):
    recipient = data_object['to']

    if type(recipient) == list:
        recipient = recipient[0]

    if recipient == 'https://www.w3.org/ns/activitystreams#Public':
        return True

    return False


def _getActorUsername(data_object):
    actor = data_object['actor']
    actor_username = re.match(URI + 'users/(.+)', actor).group(1)

    return actor_username
