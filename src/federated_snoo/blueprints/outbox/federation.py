import datetime
import requests
import json
import rsa

from flask import session
from rsa import PrivateKey
from models.account import Account
from app import db

from .activities import buildActivity
from blueprints.constants import URI


def handleFederationRequest(data_object):
    # Create the outbox entry and save it
    output_activity = buildActivity(data_object['type'], None)
    db.session.add(output_activity)
    db.session.commit()

    time = str(datetime.datetime.now())
    # Add the id to the activity object
    data_object['id'] = URI + 'outbox/' + str(output_activity.id)
    header = f'''(request-target): post /inbox \
    host: {data_object['to']} date: {time}'''

    # Create the signature
    signature = _createSignature(header)

    header_obj = {
        'Host': 'localhost',
        'Date': time,
        'Signature': signature,
        'Content-Type': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
    }

    raw_data = json.dumps(data_object)

    requests.post(data_object['to'], data=raw_data, headers=header_obj)
    return 'Sent!'


def _createSignature(header):
    account = Account.query.filter_by(
        username=session['username']
    ).first()
    private_key = account.private_key
    priv_key_obj = PrivateKey.load_pkcs1(private_key.encode('utf8'))
    signature = rsa.sign(header.encode('utf8'), priv_key_obj, 'SHA-256')
    return signature
