import json
import datetime
import uuid
import hashlib
import re

import rsa
from rsa import PublicKey, PrivateKey

from flask import Blueprint, request, session, make_response
from .constants import URI
from models.account import Account
from app import db


auth_bp = Blueprint('auth', __name__, url_prefix='/api/auth')


@auth_bp.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        data = request.form
        (private_key, public_key) = generateKeys()
        salt = uuid.uuid4().hex
        hashed_password = hashPassword(data['password'], salt)
        created_at = str(datetime.datetime.now())
        inbox_url = URI + 'inbox/'
        outbox_url = URI + 'outbox/'
        following_url = URI + 'following/'
        actor_type = 'Person'
        new_account = Account(
            username=data['username'],
            email=data['email'],
            password=hashed_password,
            salt=salt,
            private_key=private_key,
            public_key=public_key,
            created_at=created_at,
            updated_at=created_at,
            display_name='',
            inbox_url=inbox_url,
            outbox_url=outbox_url,
            following_url=following_url,
            actor_type=actor_type
        )
    db.session.add(new_account)
    db.session.commit()
    return make_response(json.dumps({'Success': True}), 201)


@auth_bp.route('/signin', methods=['GET', 'POST'])
def signin():
    if request.method == 'POST':
        data = request.form

        if isEmail(data['identifier']):
            account = Account.query.filter_by(email=data['identifier']).first()
        else:
            account = Account.query.filter_by(username=data['identifier']).first()

        if not account:
            return 'No user found with this username/email'

        salt = account.salt
        hashed_password = hashPassword(data['password'], salt)

        if hashed_password == account.password:
            print('here')
            session['id'] = account.id
            session['username'] = account.username

            print('Signed in')

        return make_response(json.dumps({'Success': True}), 201)


@auth_bp.route('/signout')
def signout():
    session.pop('username', None)
    session.pop('id', None)
    return 'Signed out'


def isEmail(string):
    match = re.match(r'.+@\w\..+$', string)
    if match:
        return True
    return False


def generateKeys():
    (pubkey, privkey) = rsa.newkeys(512)
    pubkey_export = pubkey.save_pkcs1().decode('utf8')
    privkey_export = privkey.save_pkcs1().decode('utf8')

    return (pubkey_export, privkey_export)


def hashPassword(password, salt):
    return hashlib.sha512(password.encode('utf-8') +
                          salt.encode('utf-8')).hexdigest()
