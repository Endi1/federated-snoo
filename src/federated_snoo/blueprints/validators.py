from flask import abort, request
from .outbox.helpers import getDataObject


def activity_stream_validator(func):
    def wrapper(*args, **kwargs):
        data_object = getDataObject(request)

        if '@context' not in data_object:
            abort(400)

        if data_object['@context'] != 'https://www.w3.org/ns/activitystreams':
            abort(400)

        if 'actor' not in data_object:
            abort(401)

        return func()
    return wrapper


# In the future we will accept activities from other servers, so the check here
# should be implemented cryptographically too
def user_authenticated_validator(current_session):
    if 'id' not in current_session:
        abort(401)


def content_type_validator(func):
    def wrapper(*args, **kwargs):
        content_type = request.content_type
        activity_json_string = 'application/activity+json; profile="https://www.w3.org/ns/activitystreams"'
        ld_json_string = 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
        activity_json_check = lambda s: s == activity_json_string
        ld_json_check = lambda s: s == ld_json_string

        if not activity_json_check(content_type) and not ld_json_check(content_type):
            abort(400)
        return func()
    return wrapper
