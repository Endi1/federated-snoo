from flask import Blueprint, jsonify
from models.thread_item import ThreadItem

firehose_bp = Blueprint('firehose', __name__, url_prefix='/')


@firehose_bp.route('/', methods=['GET'])
def index():
    result = []
    thread_items = ThreadItem.query.filter_by(
        parent=None
    ).order_by(
        ThreadItem.created_at.desc()
    ).limit(100).all()

    for item in thread_items:
        result.append(
            {'uri': item.uri,
             'account_uri': item.account_uri,
             'title': item.title,
             'text': item.text,
             'source': item.source,
             'posted_url': item.posted_url,
             'created_at': item.created_at,
             'updated_at': item.updated_at}
        )

    return jsonify(result)


@firehose_bp.route('/thread/<uid>', methods=['GET'])
def thread(uid):
    result = {}
    uri = 'http://localhost:8000/thread/'+uid
    main_item = ThreadItem.query.filter_by(uri=uri).first()
    result['main_item'] = _build_data(main_item)

    children = main_item.children
    for child in children:
        result['main_item']['children'].append(_traverse(child))

    return jsonify(result)


def _traverse(item):
    data = _build_data(item)
    for child in item.children:
        data['children'].append(
            _traverse(child)
        )

    return data


def _build_data(item):
    return {
        'uri': item.uri,
        'account_uri': item.account_uri,
        'title': item.title,
        'text': item.text,
        'source': item.source,
        'posted_url': item.posted_url,
        'created_at': item.created_at,
        'updated_at': item.updated_at,
        'children': []
    }
