import re
import requests
import json

import Crypto.PublicKey.RSA as RSA
from Crypto.Hash import SHA256
from flask import Blueprint, request, abort
from ..validators import activity_stream_validator, content_type_validator
from ..outbox.helpers import getDataObject
from .activities import (handleCreateActivity,
                         handleUpdateActivity,
                         handleLikeActivity,
                         handleDeleteActivity)

inbox_bp = Blueprint('inbox', __name__, url_prefix='/inbox')


@inbox_bp.route('/', methods=['POST'])
@activity_stream_validator
@content_type_validator
def inbox():
    if not _verifySignature:
        abort(401)

    data_object = getDataObject(request)

    activity_type = data_object['type']

    if activity_type == 'Create':
        return handleCreateActivity(data_object, 'Inbox')
    elif activity_type == 'Update':
        return handleUpdateActivity(data_object, 'Inbox')
    elif activity_type == 'Like':
        return handleLikeActivity(data_object, 'Inbox')
    elif activity_type == 'Delete':
        return handleDeleteActivity(data_object, 'Inbox')


def _verifySignature(request_obj):
    request_host = request_obj.headers['signature']

    # header_signature = request.headers['signature']
    # public_key = _getPublicKey(header_signature)
    # header = _getHeader(header_signature)
    # signature = _getSignature(header_signature)
    # pub_key_object = RSA.importKey(public_key)
    # digest = SHA256.new(header.encode('utf-8')).digest()
    # return pub_key_object.verify(digest, (int(signature),))


def _getSignature(header_signature):
    signature = re.search(r'signature="(.+)"', header_signature).group(1)
    return signature


def _getHeader(header_signature):
    print(header_signature)
    header = re.search(r'headers="(.+)", signature', header_signature).group(1)
    return header


def _getPublicKey(header_signature):
    key_id = re.match(r'keyId=\"(.+)\", headers', header_signature).group(1)
    r = requests.get(key_id)
    response_object = json.loads(r.text)

    return response_object['public_key']
