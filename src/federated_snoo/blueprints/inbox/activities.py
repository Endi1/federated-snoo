import datetime

from app import db
from blueprints.constants import URI
from blueprints.outbox.helpers import handleNewObject
from flask import make_response, jsonify, abort, session
from models.thread_item import ThreadItem
from models.input_stream import InputStream


def handleCreateActivity(data_object):
    """
    Handles an activity of 'Create' type.
    Keyword arguments:
    data_object -- the json object of the data to be saved
    """

    thread_item = handleNewObject(data_object)

    input_object = buildActivity('Create', thread_item,
                                 data_object=data_object,
                                 current_session=session)

    db.session.add(thread_item)
    db.session.add(input_object)
    db.session.commit()

    return createResponse(input_object)


def handleUpdateActivity(data_object, stream_type, current_session=None):
    """
    Handles an activity of 'Update' type.
    Keyword arguments:
    data_object -- the json object of the data to be saved
    """
    # If there's no current_session, this is an inbox request. We need
    # to make sure that the actor that sent the request is the owner
    # of the item. For now we need to check both because federation is
    # not implemented yet.

    object_ = data_object['object']
    object_uri = object_['uri']
    thread_item = ThreadItem.query.filter_by(uri=object_uri).first()

    if not thread_item:
        abort(404)

    updated_at = str(datetime.datetime.now())

    thread_item.text = object_['content']
    thread_item.source = object_['source']['content']
    thread_item.updated_at = updated_at

    activity = buildActivity('Update', thread_item,
                             data_object=data_object)

    db.session.add(activity)
    db.session.commit()

    return createResponse(activity)


def handleLikeActivity(data_object):
    """
    Handles an activity of 'Like' type.
    Keyword arguments:
    data_object -- the json object of the data to be saved
    """
    object_uri = data_object['object']
    thread_item = ThreadItem.query.filter_by(uri=object_uri).first()
    type_ = data_object['type']

    input_streams = InputStream.query.filter_by(
        account_uri=data_object['actor'], activity_type=type_
    ).all()

    if len(input_streams) % 2 == 0 and len(input_streams) != 0:
        _addLikeOrDislike(type_, -1, thread_item)
    else:
        _addLikeOrDislike(type_, 1, thread_item)

    activity = buildActivity(type_, thread_item,
                             data_object=data_object)

    db.session.add(activity)
    db.session.commit()

    return createResponse(activity)


def _addLikeOrDislike(type_, amount, thread_item):
    if type_ == 'Like':
        thread_item.likes += amount
    elif type_ == 'Dislike':
        thread_item.dislikes += amount


def handleDeleteActivity(data_object):
    """
    Handles an activity of 'Delete' type.
    Keyword arguments:
    data_object -- the json object of the data to be saved
    """
    object_uri = data_object['object']
    thread_item = ThreadItem.query.filter_by(uri=object_uri).first()

    thread_item.deleted = True

    activity = buildActivity('Delete',
                             thread_item,
                             data_object=data_object)
    db.session.add(activity)
    db.session.commit()

    return createResponse(activity)


def buildActivity(activity_type,
                  thread_item, data_object={}):
    """Builds the activity to be saved in the outbox

    Keyword arguments:
    stream_type -- either 'Inbox' or 'Outbox'
    activity_type -- the activity type (Create, Update, Like, etc)
    current_session -- the session cookie
    thread_item -- the thread item created or updated by this activity
    data_object -- The activity object as a python dict
    """
    created_at = str(datetime.datetime.now())
    account_id = session['id'] if session else None
    account_uri = data_object['actor'] if data_object else None

    activity = InputStream(activity_type=activity_type,
                           created_at=created_at,
                           updated_at=created_at,
                           account_id=account_id,
                           account_uri=account_uri,
                           activity_object=thread_item)
    return activity


def createResponse(input_activity):
    response = make_response(
        jsonify({'activity_id': (input_activity.id)}), 201)
    response.headers['Location'] = URI + 'inbox/' + str(input_activity.id)

    return response
